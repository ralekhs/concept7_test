<?php
$school = getData('school');
if (getData('errors')) {
    echo printErrors(getData('errors'));

    // input that's been sent to the route
    $oldData = getData('old_input');
}
?>

<h1 class="home-title">Update School</h1>

<form class="form-vertical" method="post" action="<?php echo publicUrl('school/update/' . $school->id) ?>">
    <div class="form-group">
        <label>School name:</label>
        <input class="form-control" type="text" name="school_name"
               value="<?php echo isset($oldData) ? $oldData['school_name'] : $school->school_name ?>">
    </div>
    <div class="form-group">
        <label>Year founded:</label>
        <input class="form-control" type="number" name="year_founded"
               value="<?php echo isset($oldData) ? $oldData['year_founded'] : $school->year_founded ?>">
    </div>
    <div class="form-group">
        <label>City:</label>
        <input class="form-control" type="text" name="city"
               value="<?php echo isset($oldData) ? $oldData['city'] : $school->city ?>">
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Save">
    </div>
</form>
