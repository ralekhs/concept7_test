<?php
$schools = getData('schools');
// display flash messages
if (getData('success')) {
    echo printSuccess(getData('success'));
}
if (getData('error')) {
    echo printErrors(getData('error'));
}
?>
    <h1 class="home-title">All Schools</h1>

<?php

if (count($schools)) {
    ?>
    <table class="table table-hover">
        <thead>
        <tr class="info">
            <th>School name</th>
            <th>Year founded</th>
            <th>City</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($schools as $school) {
            ?>
            <tr>
                <td><?php echo $school->school_name ?></td>
                <td><?php echo $school->year_founded ?></td>
                <td><?php echo $school->city ?></td>
                <td>
                    <a href="<?php echo publicUrl('school/edit/' . $school->id) ?>" class="btn btn-primary pull-left">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <form class="prompt-school-delete pull-left" method="post" action="<?php echo publicUrl('school/delete/' . $school->id) ?>">
                        <button class="btn btn-danger" type="submit">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </form>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <a class="btn btn-primary" href="<?php echo publicUrl('school/create') ?>">New school</a>
<?php
} else {
    ?>
    <div class="alert alert-warning">
        There are no schools at the moment. Create new school <a href="<?php echo publicUrl('school/create') ?>">here</a>.
    </div>
<?php
}