<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Ratko Buncic">
    <title>Concept7 Test - Ratko Buncic</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo publicUrl('css/style.css') ?>">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 top-header">
            <div class="row">
                <div class="col-md-offset-3 col-md-6">
                    <h1>
                        Concept7 Test - Ratko Buncic
                    </h1>
                </div>
            </div>
        </div>
        <div class="col-md-offset-3 col-md-6 padding-top padding-bottom">
            <div class="row">
                <div class="col-md-3">
                    <ul class="nav nav-pills nav-stacked">
                        <?php
                        if (checkAuth()) {
                            ?>
                            <li <?php echo is_numeric(strpos(getData('view'), 'school')) ? 'class="active"' : '' ?>>
                                <a href="<?php echo publicUrl('school') ?>">School</a>
                            </li>
                            <li <?php echo is_numeric(strpos(getData('view'), 'teacher')) ? 'class="active"' : '' ?>>
                                <a href="<?php echo publicUrl('teacher') ?>">Teacher</a>
                            </li>
                            <?php
                        }
                        ?>
                        <li <?php echo getData('view') == 'login' ? 'class="active"' : '' ?>>
                            <?php
                            if (checkAuth()) {
                                ?>
                                <a href="<?php echo publicUrl('logout') ?>">Logout</a>
                                <?php
                            } else {
                                ?>
                                <a href="<?php echo publicUrl('login') ?>">Login</a>
                                <?php
                            }
                            ?>
                        </li>
                        <?php
                        if (!checkAuth()) {
                            ?>
                            <li <?php echo getData('view') == 'register' ? 'class="active"' : '' ?>>
                                <a href="<?php echo publicUrl('register') ?>">Register</a>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                    if (getData('name')) {
                        ?>
                        <div class="alert alert-success user-signin">
                            <i>Welcome <b><?php echo getData('name') ?></b>!</i>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-9">
                    <?php
                    // Here we render particular views
                    getView(getData('view'));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script type="application/javascript">
    $(function(){
        $(document).on('submit', '.prompt-school-delete', function(e){
            if (!confirm('Are you sure you wanna delete this school?')) {
                e.preventDefault();
            }
        });
        $(document).on('submit', '.prompt-teacher-delete', function(e){
            if (!confirm('Are you sure you wanna delete your team?')) {
                e.preventDefault();
            }
        });
    });
</script>
</body>
</html>