<?php
$teachers = getData('teachers');
// display flash messages
if (getData('success')) {
    echo printSuccess(getData('success'));
}
if (getData('error')) {
    echo printErrors(getData('error'));
}
?>
    <h1 class="home-title">Search Teachers</h1>

<?php

if (count($teachers)) {
    ?>
    <table class="table table-hover">
        <thead>
        <tr class="info">
            <th>First Name</th>
            <th>Last Name</th>
            <th>Birth Date</th>
            <th>School Name</th>
            <th>
                <form action="<?php echo publicUrl('teacher/search')?>">
                    <div class="input-group">
                        <input type="text" class="form-control" name="q" placeholder="Search..." value="<?php echo isset($_REQUEST['q']) ? $_REQUEST['q'] : '' ?>">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-success" type="button">Search</button>
                        </span>
                    </div>
                </form>
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($teachers as $teacher) {
            ?>
            <tr>
                <td><?php echo $teacher->first_name ?></td>
                <td><?php echo $teacher->last_name ?></td>
                <td><?php echo $teacher->birth_date ?></td>
                <td><?php echo isset($teacher->school()->school_name) ? $teacher->school()->school_name : '' ?></td>
                <td>
                    <a href="<?php echo publicUrl('teacher/edit/' . $teacher->id) ?>" class="btn btn-primary pull-left">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                    <form class="prompt-school-delete pull-left" method="post"
                          action="<?php echo publicUrl('teacher/delete/' . $teacher->id) ?>">
                        <button class="btn btn-danger" type="submit">
                            <span class="glyphicon glyphicon-remove"></span>
                        </button>
                    </form>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
    <a class="btn btn-primary" href="<?php echo publicUrl('teacher/create') ?>">New teacher</a>
    <?php
} else {
    ?>
    <div class="alert alert-warning">
        There are no teachers for the given search term. Go back to <a href="<?php echo publicUrl('teacher') ?>">teachers page</a>.
    </div>
    <?php
}