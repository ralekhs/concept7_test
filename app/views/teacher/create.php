<?php
if (getData('errors')) {
    echo printErrors(getData('errors'));

    // input that's been sent to the route
    $oldData = getData('old_input');
}
?>

<h1 class="home-title">New Teacher</h1>

<form class="form-vertical" method="post" action="<?php echo publicUrl('teacher/create') ?>">
    <div class="form-group">
        <label>First name:</label>
        <input class="form-control" type="text" name="first_name"
               value="<?php echo isset($oldData) ? $oldData['first_name'] : '' ?>">
    </div>
    <div class="form-group">
        <label>Last name:</label>
        <input class="form-control" type="text" name="last_name"
               value="<?php echo isset($oldData) ? $oldData['last_name'] : '' ?>">
    </div>
    <div class="form-group">
        <label>Birth date:</label>
        <input class="form-control" type="text" name="birth_date"
               value="<?php echo isset($oldData) ? $oldData['birth_date'] : '' ?>">
    </div>
    <div class="form-group">
        <label>School:</label>
        <select class="form-control" name="school">
            <option></option>
            <?php
            foreach (getData('schools') as $school) {
                $selected = isset($oldData) && $oldData['school'] == $school->id ? 'selected' : '';
                echo "<option value='{$school->id}' {$selected}>{$school->school_name}</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Save">
    </div>
</form>
