<?php
// display flash messages
if (getData('errors')) {
    echo printErrors(getData('errors'));
    
    // input that's been sent to the route
    $oldData = getData('old_input');
}
?>
<form class="form-vertical" method="post" action="<?php echo publicUrl('register') ?>">
    <div class="form-group">
        <label>Email:</label>
        <input class="form-control" type="text" name="email" value="<?php echo isset($oldData) ? $oldData['email'] : ''?>">
    </div>
    <div class="form-group">
        <label>Password:</label>
        <input class="form-control" type="password" name="password" value="">
    </div>
    <div class="form-group">
        <label>Repeat password:</label>
        <input class="form-control" type="password" name="password_repeat">
    </div>
    <div class="form-group">
        <input class="btn btn-primary" type="submit" value="Register">
    </div>
</form>