<?php

namespace App\Core\Routing;

/**
 * Route that will be requested.
 * 
 * @property string $uri Part of URL that we want to visit
 * @property string $method GET or POST
 * @property string $function Callback function that we want to execute
 * @property boolean $authNeeded If route require user to be logged in
 */
class Route {

    private $uri;
    private $method;
    private $function;
    private $authNeeded;

    public function getUri() {
        return $this->uri;
    }

    public function setUri($uri) {
        $this->uri = $uri;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->method = $method;
    }

    public function getFunction() {
        return $this->function;
    }

    public function setFunction($function) {
        $this->function = $function;
    }

    public function getAuthNeeded()
    {
        return $this->authNeeded;
    }

    public function setAuthNeeded($authNeeded)
    {
        $this->authNeeded = $authNeeded;
    }

}
