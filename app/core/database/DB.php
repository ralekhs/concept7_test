<?php

namespace App\Core\Database;

use PDO, Exception;

/**
 * DB is used for raw db connection and query execution.
 * Realized as singleton, to make sure we always get the same instance.
 */
class DB
{
    private static $instance;
    private $conn;

    private function __construct()
    {
        $this->_setConnection();
    }

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Used for SELECT sql queries.
     * @param $tableName
     * @param $primaryKey
     * @param array|null $select
     * @param array|null $where
     * @param array|null $orderBy
     * @param null $limit
     * @return mixed
     * @throws Exception
     */
    public function select($tableName, $primaryKey, array $select = null, array $where = null, array $orderBy = null, $limit = null)
    {
        $selectNames = "*";

        if (count($select)) {
            $selectNames = implode(',', $select);
        }

        $sqlQuery = "SELECT {$selectNames} FROM {$tableName}";
        $conditions = [];
        $orderDefault = " ORDER BY {$primaryKey}";
        $order = '';

        if (!empty($where)) {
            foreach ($where as $condition) {
                if (is_array($condition) && count($condition) == 3) {
                    $conditions[] = "{$condition[0]} {$condition[1]} '{$condition[2]}'";
                    continue;
                }

                throw new Exception('Condition must be an array with 3 values.');
            }
        }

        if (count($conditions)) {
            $sqlQuery .= ' WHERE ' . implode(' AND ', $conditions);
        }

        if (!empty($orderBy)) {

            $order = ' ORDER BY ';

            if (!empty($orderBy[0])) {
                $order .= $orderBy[0];
            }

            if (!empty($orderBy[1])) {
                if (in_array(strtolower($orderBy[1]), ['asc', 'desc'])) {
                    $order .= " {$orderBy[1]}";
                }
            }
        }

        $sqlQuery .= $order != '' ? $order : $orderDefault;

        if (is_numeric($limit)) {
            if ($limit > 0) {
                $sqlQuery .= " LIMIT {$limit}";
            }
        } else if (is_array($limit)) {
            if (count($limit) == 1) {
                $sqlQuery .= " LIMIT {$limit[0]}";
            } else if (count($limit) == 2) {
                $sqlQuery .= " LIMIT {$limit[0]},{$limit[1]}";
            }
        }

        $sqlQuery .= ';';

//        dd($sqlQuery);

        if ($result = $this->conn->query($sqlQuery)) {
            return $result->fetchAll(PDO::FETCH_CLASS);
        }

        return null;
    }

    /**
     * Used for fully manual and custom sql queries
     * @param $sqlQuery
     * @param $class
     * @return mixed
     */
    public function selectRaw($sqlQuery, $class = null)
    {
        if ($result = $this->conn->query($sqlQuery)) {
            if ($class) {
                return $result->fetchAll(PDO::FETCH_CLASS, $class);
            }
            return $result->fetchAll(PDO::FETCH_CLASS);
        }

        return null;
    }

    /**
     * Used for INSERT and UPDATE sql queries
     * @param $tableName
     * @param $primaryKey
     * @param $attributes
     * @return bool
     */
    public function save($tableName, $primaryKey, $attributes)
    {
        $isUpdate = isset($attributes[$primaryKey]);

        if ($isUpdate) {
            $sqlQuery = $this->_updateSql($tableName, $primaryKey, $attributes);
        } else {
            $sqlQuery = $this->_insertSql($tableName, $attributes);
        }

        if ($this->conn->exec($sqlQuery)) {
            return true;
        }

        return false;
    }

    /**
     * Used for DELETE sql queries
     * @param $tableName
     * @param $primaryKey
     * @param $value
     * @return bool
     */
    public function delete($tableName, $primaryKey, $value)
    {
        $sqlQuery = "DELETE FROM {$tableName} WHERE {$primaryKey} = {$value};";
        if ($this->conn->exec($sqlQuery)) {
            return true;
        }

        return false;
    }

    /**
     * Sets DB connection, PDO is used for that
     */
    private function _setConnection()
    {
        $dbname = config('db_name');
        $dbhost = config('db_host');
        $dbusername = config('db_user');
        $dbpassword = config('db_pass');

        $this->conn = new PDO("mysql:dbname={$dbname};host={$dbhost}", $dbusername, $dbpassword);
        $this->conn->errorInfo();
    }

    /**
     * Form INSERT sql query and returns it
     * @param $tableName
     * @param $attributes
     * @return string
     */
    private function _insertSql($tableName, $attributes)
    {
        $selectNames = implode(',', array_keys($attributes));
        $selectValues = implode('","', array_values($attributes));
        $sqlQuery = "INSERT INTO {$tableName} ({$selectNames}) VALUES (\"{$selectValues}\")";
        return $this->_fixSqlNull($sqlQuery);
    }

    /**
     * Form UPDATE sql query and returns it
     * @param $tableName
     * @param $primaryKey
     * @param $attributes
     * @return string
     */
    private function _updateSql($tableName, $primaryKey, $attributes)
    {
        $where = "WHERE {$primaryKey} = {$attributes[$primaryKey]}";
        unset($attributes[$primaryKey], $attributes['created_at'], $attributes['updated_at']);

        $sqlQuery = "UPDATE {$tableName} SET ";

        $update = [];

        foreach ($attributes as $key => $value) {
            $update[] = "{$key} = \"{$value}\"";
        }

        $sqlQuery .= implode(',', $update) . " {$where}";

        return $this->_fixSqlNull($sqlQuery);
    }

    /**
     * Replace empty string in sql query for a NULL
     * @param $sqlQuery
     * @return mixed
     */
    private function _fixSqlNull($sqlQuery)
    {
        return str_replace('""','NULL', $sqlQuery);
    }

}