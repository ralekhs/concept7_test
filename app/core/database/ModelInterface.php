<?php

namespace App\Core\Database;

/**
 * Model interface that defines what abstract Model should contain
 */
interface ModelInterface
{
    public static function find($data);

    public static function findAll($data = null, array $orderBy = null, $limit = null);

    public static function select(array $select = null, array $where = null, array $order = null, $limit = null);

    public function save();

    public function delete();
}