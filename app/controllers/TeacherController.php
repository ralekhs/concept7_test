<?php

namespace App\Controllers;

use App\Models\School, App\Models\Teacher, App\Validators\Validator;

class TeacherController
{
    private $validator;

    function __construct()
    {
        $this->validator = Validator::instance();
    }

    public function index()
    {
        $teachers = Teacher::findAll(null, null, 5);
        return view('teacher/list', compact('teachers'));
    }

    public function create()
    {
        $schools = School::findAll(null, ['school_name']);
        return view('teacher/create', compact('schools'));
    }

    public function store()
    {
        $data = input();

        $errors = $this->validator->validateTeacher($data);
        if (count($errors)) {
            return view('teacher/create', [
                'errors' => $errors,
                'old_input' => $data,
                'schools' => $schools = School::findAll(null, ['school_name'])
            ]);
        }

        $teacher = new Teacher();
        $teacher->first_name = $data['first_name'];
        $teacher->last_name = $data['last_name'];
        $teacher->birth_date = $data['birth_date'];
        $teacher->school_id = $data['school'];
        $teacher->full_name = $data['first_name'] . ' ' . $data['last_name'];

        if ($teacher->save()) {
            return redirect('teacher', ['success' => 'You have successfully created new teacher.']);
        }

        return redirect('teacher/create', ['errors' => 'Something went wrong with creating new teacher.', 'old_input' => $data]);
    }

    public function edit($id)
    {
        $teacher = Teacher::find($id);
        $schools = School::findAll(null, ['school_name']);

        if (is_null($teacher)) {
            return redirect('teacher', ['error' => 'There is no wanted teacher.']);
        }

        return view('teacher/edit', compact('teacher', 'schools'));
    }

    public function update($id)
    {
        $teacher = Teacher::find($id);

        if (is_null($teacher)) {
            return redirect('teacher', ['error' => 'There is no wanted teacher.']);
        }

        $data = input();

        $errors = $this->validator->validateTeacher($data);
        if (count($errors)) {
            return redirect('teacher/edit/' . $teacher->id, ['errors' => $errors, 'old_input' => $data]);
        }

        $teacher->first_name = $data['first_name'];
        $teacher->last_name = $data['last_name'];
        $teacher->birth_date = $data['birth_date'];
        $teacher->school_id = $data['school'];
        $teacher->full_name = $data['first_name'] . ' ' . $data['last_name'];

        if ($teacher->save()) {
            return redirect('teacher', ['success' => 'You have successfully updated teacher.']);
        }

        return redirect('teacher/edit/' . $teacher->id, ['errors' => 'Something went wrong with updating teacher.', 'old_input' => $data]);
    }

    public function delete($id)
    {
        $teacher = Teacher::find($id);

        if (is_null($teacher)) {
            return redirect('teacher', ['error' => 'There is no wanted teacher.']);
        }

        if ($teacher->delete()) {
            return redirect('teacher', ['success' => 'You have successfully deleted teacher.']);
        }

        return redirect('teacher', ['error' => 'Something went wrong with deleting teacher.']);
    }

    public function search()
    {
        $q = input('q');

        if (empty($q)) {
            $teachers = [];
            return view('teacher/search', compact('teachers'));
        }

        $condition = [
            [
                'full_name',
                'like',
                "{$q}%"
            ]
        ];

        $teachers = Teacher::findAll($condition);

        return view('teacher/search', compact('teachers'));
    }
}