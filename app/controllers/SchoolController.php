<?php

namespace App\Controllers;

use App\Models\School, App\Validators\Validator;

class SchoolController
{
    private $validator;

    function __construct()
    {
        $this->validator = Validator::instance();
    }

    public function index()
    {
        $schools = School::findAll(null, null, 5);
        return view('school/list', compact('schools'));
    }

    public function create()
    {
        return view('school/create');
    }

    public function store()
    {
        $data = input();

        $errors = $this->validator->validateSchool($data);
        if (count($errors)) {
            return view('school/create', ['errors' => $errors, 'old_input' => $data]);
        }

        $school = new School();
        $school->school_name = $data['school_name'];
        $school->year_founded = $data['year_founded'];
        $school->city = $data['city'];

        if ($school->save()) {
            return redirect('school', ['success' => 'You have successfully created new school.']);
        }

        return redirect('school/create', ['error' => 'Something went wrong with creating new school.', 'old_input' => $data]);
    }

    public function edit($id)
    {
        $school = School::find($id);

        if (is_null($school)) {
            return redirect('school', ['error' => 'There is no wanted school.']);
        }

        return view('school/edit', compact('school'));
    }

    public function update($id)
    {
        $school = School::find($id);

        if (is_null($school)) {
            return redirect('school', ['error' => 'There is no wanted school.']);
        }

        $data = input();

        $errors = $this->validator->validateSchool($data);
        if (count($errors)) {
            return redirect('school/edit/' . $school->id, ['errors' => $errors, 'old_input' => $data]);
        }

        $school->school_name = $data['school_name'];
        $school->year_founded = $data['year_founded'];
        $school->city = $data['city'];

        if ($school->save()) {
            return redirect('school', ['success' => 'You have successfully updated school.']);
        }

        return redirect('school/edit/' . $school->id, ['error' => 'Something went wrong with updating school.', 'old_input' => $data]);
    }

    public function delete($id)
    {
        $school = School::find($id);

        if (is_null($school)) {
            return redirect('school', ['error' => 'There is no wanted school.']);
        }

        if ($school->delete()) {
            return redirect('school', ['success' => 'You have successfully deleted school.']);
        }

        return redirect('school', ['error' => 'Something went wrong with deleting school.']);
    }
}