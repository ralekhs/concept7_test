<?php

namespace App\Controllers;

class HomeController
{
    /**
     * Home page render
     * In this case redirects to the school page
     */
    public function index()
    {
        return redirect('school');
    }

}
