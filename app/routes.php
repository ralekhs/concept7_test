<?php

/**
 * Here we add our routes.
 */

use App\Core\Routing\Router;

$router = new Router();

$router->get('/', 'HomeController@index', true);

$router->get('/login', 'UserController@login');
$router->post('/login', 'UserController@postLogin');

$router->get('/logout', 'UserController@logout', true);

$router->get('/register', 'UserController@register');
$router->post('/register', 'UserController@postRegister');

$router->get('/school', 'SchoolController@index', true);

$router->get('/school/create', 'SchoolController@create', true);
$router->post('/school/create', 'SchoolController@store', true);

$router->get('/school/edit', 'SchoolController@edit', true);
$router->post('/school/update', 'SchoolController@update', true);

$router->post('/school/delete', 'SchoolController@delete', true);

$router->get('/teacher', 'TeacherController@index', true);

$router->get('/teacher/create', 'TeacherController@create', true);
$router->post('/teacher/create', 'TeacherController@store', true);

$router->get('/teacher/edit', 'TeacherController@edit', true);
$router->post('/teacher/update', 'TeacherController@update', true);

$router->post('/teacher/delete', 'TeacherController@delete', true);

$router->get('/teacher/search', 'TeacherController@search', true);