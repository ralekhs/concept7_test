<?php

namespace App\Validators;

use DateTime;

/**
 * Validator contains methods that handle validation of inputs
 * Realized as singleton, to make sure we always get the same instance.
 */
class Validator
{
    private static $validator;

    private function __construct(){}

    public static function instance()
    {
        if (is_null(self::$validator)) {
            self::$validator = new self();
        }
        return self::$validator;
    }

    /**
     * Validate registration inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validateRegistration($data)
    {
        $errors = [];

        if (array_key_exists('email', $data)) {
            $email = $data['email'];
            if ($email != '') {
                if (!strpos($email, '@')) {
                    $errors[] = 'Email is not valid.';
                }
            } else {
                $errors[] = 'Email can\'t be empty value.';
            }
        } else {
            $errors[] = 'Email is required.';
        }

        if (array_key_exists('password', $data)) {
            if (array_key_exists('password_repeat', $data)) {
                $password = $data['password'];
                $passwordRepeat = $data['password_repeat'];
                if ($password != '') {
                    if ($passwordRepeat != '') {
                        if ($password != $passwordRepeat) {
                            $errors[] = 'Password repeat must be same as password.';
                        }
                    } else {
                        $errors[] = 'Password repeat can\'t be empty value.';
                    }
                } else {
                    $errors[] = 'Password can\'t be empty value.';
                    if ($passwordRepeat == '') {
                        $errors[] = 'Password repeat can\'t be empty value.';
                    }
                }
            } else {
                $errors[] = 'Password repeat is required.';
            }
        } else {
            $errors[] = 'Password is required.';
            if (!array_key_exists('password_repeat', $data)) {
                $errors[] = 'Password repeat is required.';
            }
        }

        return $errors;
    }

    /**
     * Validate login inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validateLogin($data)
    {
        $errors = [];
        if (array_key_exists('email', $data)) {
            $email = $data['email'];
            if ($email != '') {
                if (!strpos($email, '@')) {
                    $errors[] = 'Email is not valid.';
                }
            } else {
                $errors[] = 'Email can\'t be empty value.';
            }
        } else {
            $errors[] = 'Email is required.';
        }

        if (array_key_exists('password', $data)) {
            $password = $data['password'];
            if ($password == '') {
                $errors[] = 'Password can\'t be empty value.';
            }
        } else {
            $errors[] = 'Password is required.';
        }

        return $errors;
    }

    /**
     * Validate school creation and update inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validateSchool($data)
    {
        $errors = [];

        if (array_key_exists('school_name', $data)) {
            $school_name = $data['school_name'];
            if ($school_name == '') {
                $errors[] = 'School name can\'t be empty.';
            }
        } else {
            $errors[] = 'Name is required.';
        }

        if (array_key_exists('year_founded', $data)) {
            $year_founded = $data['year_founded'];
            if ($year_founded != '') {
                if (is_numeric($year_founded)) {
                    if ($year_founded < 1) {
                        $errors[] = 'Year founded can\'t be 0.';
                    }
                } else {
                    $errors[] = 'Year founded must be integer.';
                }
            } else {
                $errors[] = 'Year founded can\'t be empty.';
            }
        } else {
            $errors[] = 'Year founded is required.';
        }

        return $errors;
    }

    /**
     * Validate teacher creation and update inputs and return array with possible errors.
     *
     * @param array $data
     * @return array
     */
    public function validateTeacher($data)
    {
        $errors = [];

        if (array_key_exists('first_name', $data)) {
            $first_name = $data['first_name'];
            if ($first_name == '') {
                $errors[] = 'First name can\'t be empty.';
            }
        } else {
            $errors[] = 'First name is required.';
        }

        if (array_key_exists('last_name', $data)) {
            $last_name = $data['last_name'];
            if ($last_name == '') {
                $errors[] = 'Last name can\'t be empty.';
            }
        } else {
            $errors[] = 'First name is required.';
        }

        if (array_key_exists('birth_date', $data)) {
            $birth_date = $data['birth_date'];
            if ($birth_date != '') {
                if (!DateTime::createFromFormat('Y-m-d', $birth_date)) {
                    $errors[] = 'Birth date must be in format Y-m-d.';
                }
            } else {
                $errors[] = 'Birth date can\'t be empty.';
            }
        } else {
            $errors[] = 'Birth date is required.';
        }

        if (array_key_exists('school', $data)) {
            $school = $data['school'];
            if ($school == '') {
                $errors[] = 'School can\'t be empty.';
            }
        } else {
            $errors[] = 'School is required.';
        }

        return $errors;
    }

}
