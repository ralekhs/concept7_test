<?php

namespace App\Models;

use App\Core\Database\Model;

class Teacher extends Model
{
    protected $tableName = 'teachers';

    public function school()
    {
        return School::find($this->school_id);
    }
}