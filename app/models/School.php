<?php

namespace App\Models;

use App\Core\Database\Model;

class School extends Model
{
    protected $tableName = 'schools';
}