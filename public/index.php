<?php

/**
 * Starting our app, get class autoloader, inject routes and and execute URI.
 * On the end clear all from session flash data.
 */
session_start();

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../app/routes.php';

$router->process();
removeFlashData();